package id.co.asyst.amala.redeempromo.rule.getpromousage.utils;

import com.google.gson.Gson;
import id.co.asyst.commons.utils.constant.CommonsConstants;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Jul 15, 2021
 * @since 1.2
 */

public class Utils {
    private static final Gson gson = new Gson();
    private static final Logger logger = LogManager.getLogger();

    public void inventory(Exchange exchange) {
        List<Map<String, Object>> resultData = new ArrayList<>();
        exchange.setProperty("responseresult", resultData);
    }

    public void validateRequest(Exchange exchange) {
        Map<String, Object> requestdata = exchange.getProperty("requestdata", Map.class);
        List<String> promocodeList = new ArrayList<>();
        String[] parameter = {"promocode","memberid"};
        String resmsg = "";
        String parameterVal = "";
        try {
            for (int i = 0; i < parameter.length; i++) {
                if (StringUtils.isEmpty(resmsg)) {
                    parameterVal = parameter[i];
                    if (StringUtils.isEmpty(requestdata.get(parameterVal)) || requestdata.get(parameterVal) == null)
                        resmsg = parameterVal+" is required";
                    else {
                        if (parameterVal.equals("promocode")) {
                            String promocodeReplace = requestdata.get(parameter[i]).toString().replace("[","").replace("]", "").replace(" ","");
                            if (promocodeReplace.length() > 0) {
                                String[] promocode = promocodeReplace.split(",");
                                for (int j = 0; j < promocode.length; j++) {
                                    promocodeList.add(promocode[j]);
                                }
                            } else {
                                resmsg = parameterVal+" is required";
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            resmsg = parameterVal+" is required";
        }
        exchange.setProperty("resMsg", resmsg);
        exchange.setProperty("promocodelist", promocodeList);
    }

    public void resultResponseData(Exchange exchange) {
        List<Map<String, Object>> responseresult = exchange.getProperty("responseresult", List.class);
        List<Map<String, Object>> promocatalogresult = exchange.getProperty("promocatalogresult", List.class);
        List<Map<String, Object>> participantlimitresult = exchange.getProperty("participantlimit", List.class);
        List<Map<String, Object>> memberuselimitresult = exchange.getProperty("memberuselimit", List.class);

        Map<String, Object> data = new HashMap<>();
        try {
            int participantlimitpromo = Integer.parseInt(promocatalogresult.get(0).get("participantlimit").toString());
            int memberuselimitpromo = Integer.parseInt(promocatalogresult.get(0).get("memberuselimit").toString());
            int participantlimit = Integer.parseInt(participantlimitresult.get(0).get("participantlimit").toString());
            int memberuselimit = Integer.parseInt(memberuselimitresult.get(0).get("memberuselimit").toString());
            Boolean status = Boolean.FALSE;
            Boolean participantlimitunlimited = false;
            Boolean memberuselimitunlimited = false;
            if (participantlimitpromo == 0) {
                if (memberuselimitpromo == 0) {
                    status = Boolean.TRUE;
                    memberuselimitunlimited = true;
                    data.put("participantlimit", 0);
                    data.put("memberuselimit", 0);
                } else {
                    if (memberuselimitpromo > memberuselimit) {
                        status = Boolean.TRUE;
                        data.put("participantlimit", 0);
                        data.put("memberuselimit", memberuselimitpromo-memberuselimit);
                    } else {
                        data.put("participantlimit", 0);
                        data.put("memberuselimit", 0);
                    }
                }
                /*participantlimit unlimited*/
                participantlimitunlimited = true;
            } else {
                if (memberuselimitpromo == 0) {
                    /*memberuselimitunlimited*/
                    memberuselimitunlimited = true;
                    if (participantlimitpromo > participantlimit) {
                        status = Boolean.TRUE;
                        data.put("participantlimit", participantlimitpromo-participantlimit);
                        data.put("memberuselimit", 0);
                    } else {
                        data.put("participantlimit", participantlimitpromo-participantlimit);
                        data.put("memberuselimit", 0);
                    }
                } else {
                    if ((participantlimitpromo > participantlimit) && (memberuselimitpromo > memberuselimit)) {
                        status = Boolean.TRUE;
                        data.put("participantlimit", participantlimitpromo-participantlimit);
                        data.put("memberuselimit", memberuselimitpromo-memberuselimit);
                    } else {    
                        data.put("participantlimit", 0);
                        data.put("memberuselimit", 0);
                    }
                }
            }
            data.put("status", status);
            data.put("promocode",promocatalogresult.get(0).get("promocode"));
            data.put("partisipantunlimited", participantlimitunlimited);
            data.put("memberunlimited", memberuselimitunlimited);
        } catch (Exception e) {
            logger.info("promocode "+promocatalogresult.get(0).get("promocode")+ " error");
        }
        responseresult.add(data);
        exchange.setProperty("responseresult", responseresult);
    }

    public void responseSuccess(Exchange exchange) {
        List<Map> responseresult = exchange.getProperty("responseresult", List.class);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put(CommonsConstants._SERVICE_IDENTITY_KEY, exchange.getProperty("identity", Map.class));
        logger.info("data ::: " + responseresult);
        response.put(CommonsConstants._RESPONSE_RESULT_KEY, responseresult);
        response.put(CommonsConstants._RESPONSE_STATUS, status(exchange));
        exchange.getOut().setBody(gson.toJson(response));
    }

    public void responseFailed(Exchange exchange) {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put(CommonsConstants._SERVICE_IDENTITY_KEY, exchange.getProperty("identity", Map.class));
        response.put(CommonsConstants._RESPONSE_STATUS, status(exchange));
        exchange.getOut().setBody(gson.toJson(response));
    }

    @SuppressWarnings("unused")
    private Map<String, Object> status(Exchange exchange) {
        Map<String, Object> status = new HashMap<String, Object>();
        String rescode = exchange.getProperty("rescode", String.class);
        String resdesc = exchange.getProperty("resdesc", String.class);
        String resmsg = exchange.getProperty("resmsg", String.class);


        status.put(CommonsConstants._RESPONSE_CODE, rescode);
        status.put(CommonsConstants._RESPONSE_DESC, resdesc);
        status.put(CommonsConstants._RESPONSE_MSG, resmsg);

        return status;
    }
}

